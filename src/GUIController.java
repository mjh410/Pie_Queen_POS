/**
 * File Name: GUIController.java
 * Date: April 13, 2018
 * Author: Matt Huffman
 * Course: CMSC 495
 * Assignment: Final Project
 * Purpose: This file is a controller for the GUI card layout. It handles the main content frame and the switch
 * method to switch cards. The class variables and switchPanel method are static to be accessible from other classes
 * without creating multiple instances of this class.
 * Created Using: IntelliJ IDEA
 */

import javax.swing.*;
import java.awt.*;


class GUIController {

    private static JFrame frame = new JFrame();
    private static JPanel mainPanel;
    private static CardLayout card;

    GUIController() {
        frame.setTitle("Pie Queen POS");
        frame.setResizable(false);
        frame.setSize(new Dimension(1366, 768));
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        card = new CardLayout();
        mainPanel = new JPanel();
        mainPanel.setLayout(card);
        addAllPanels();
        frame.setContentPane(mainPanel);
        frame.setVisible(true);
        frame.pack();
    }

    static void switchPanel(Container container, String panelName) {
        card.show(container, panelName);
    }

    static void addAllPanels() {
        mainPanel.add(new LogOnGUI().getMainPanel(), "Log On");
        mainPanel.add(new MainMenuGUI().getMainPanel(), "Main Menu");
        mainPanel.add(new OrderMenuGUI().getOrderMenuPanel(), "Order Menu");
        mainPanel.add(new OrderMenuGUI().getCreateNewOrderPanel(), "Create Order Menu");
        mainPanel.add(new OrderMenuGUI().getItemMenuPanel(), "Item Menu");
        mainPanel.add(new OrderMenuGUI().getAddItemPanel(), "Add Item");
        mainPanel.add(new OrderMenuGUI().getRemoveItemPanel(), "Remove Item");
        mainPanel.add(new OrderMenuGUI().getPaymentPanel(), "Payment");
        mainPanel.add(new ResMenuGUI().getResMenuPanel(), "Reservation Menu");
        mainPanel.add(new ResMenuGUI().getCreateResPanel(), "Create Res");
        mainPanel.add(new ResMenuGUI().getModifyResPanel(), "Modify Res");
        mainPanel.add(new ResMenuGUI().getDeleteResPanel(), "Delete Res");
        mainPanel.add(new ResMenuGUI().getConfirmDeleteResPanel(), "Confirm Delete Res");
        mainPanel.add(new ResMenuGUI().getViewAllPanel(), "View All Res");
        mainPanel.add(new MgmtMenuGUI().getMgmtMenuPanel(), "Management Menu");
        mainPanel.add(new MgmtMenuGUI().getAddUserPanel(), "Mgmt Add User");
        mainPanel.add(new MgmtMenuGUI().getDisableUserPanel(), "Mgmt Disable User");
        mainPanel.add(new MgmtMenuGUI().getReportsPanel(), "Reports Menu");
        mainPanel.add(new MgmtMenuGUI().getDailySalesPanel(), "Daily Report");
        mainPanel.add(new MgmtMenuGUI().getWeeklySalesPanel(), "Weekly Report");
        mainPanel.add(new MgmtMenuGUI().getMonthlySalesPanel(), "Monthly Report");
        mainPanel.add(new MgmtMenuGUI().getMenuReportPanel(), "Menu Report");
        mainPanel.add(new MgmtMenuGUI().getConfirmDisableUserPanel(), "Confirm Disable User");
    }

    static void updatePanel(JPanel panel, String s) {
        mainPanel.add(panel, s);
    }

    static JPanel getMainPanel() {
        return mainPanel;
    }
}