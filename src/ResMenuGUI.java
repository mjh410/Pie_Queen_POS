/**
 * File Name: ResMenuGUI.java
 * Date: April 06, 2018
 * Author: Matt Huffman and Trish Sims
 * Course: CMSC 495
 * Assignment: Final Project
 * Purpose: This file contains the code for the reservation menu GUI.
 * Created Using: IntelliJ IDEA
 */

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.event.*;

public class ResMenuGUI extends JPanel {
    private JPanel mainGUIPanel;

    // Main Reservation Menu Components
    private JPanel resMenuPanel;
    private JPanel resMenuLeftPanel;
    private JPanel resMenuRightPanel;
    private JButton resMenuCreateResButton;
    private JButton resMenuModifyResButton;
    private JButton resMenuDeleteResButton;
    private JButton resMenuViewAllButton;
    private JButton resMenuBackButton;
    private JLabel resMenuLabel;

    // Create Reservation Menu Components
    private JPanel createResPanel;
    private JTextField createCustNameTextField;
    private JTextField createTimeTextField;
    private JTextField createTableNumTextField;
    private JTextField createNumGuestsTextField;
    private JLabel createCustNameLabel;
    private JLabel createTimelabel;
    private JLabel createTableNumLabel;
    private JLabel createNumGuestsLabel;
    private JLabel createDateLabel;
    private JTextField createDateTextField;
    private JButton createBackButton;
    private JButton createSubmitButton;
    private JLabel createResLabel;

    // Modify Reservation Menu Components
    private JPanel modifyResPanel;
    private JPanel modifyRightPanel;
    private JPanel modifyLeftPanel;
    private JScrollPane modifyScrollPane;
    private JTable modifySelectResTable;
    private JLabel modifySelectResLabel;
    private JLabel modifyCustNameLabel;
    private JLabel modifyResTimeLabel;
    private JLabel modifyTableNumLabel;
    private JLabel modifyNumGuestsLabel;
    private JLabel modifyResIdLabel;
    private JTextField modifyCustNameTextField;
    private JTextField modifyResTimeTextField;
    private JTextField modifyTableNumTextField;
    private JTextField modifyNumGuestsTextField;
    private JTextField modifyResDateTextField;
    private JTextField modifyResIdTextField;
    private JLabel modifyResDateLabel;
    private JButton modifyBackButton;
    private JButton modifySubmitButton;
    private JLabel modifyResLabel;

    // Delete Reservation Menu Components
    private JPanel deleteResPanel;
    private JComboBox<String> delResSelectComboBox;
    private JButton delDeleteButton;
    private JButton delCancelButton;
    private JLabel delResSelectLabel;
    private JLabel delResLabel;

    // View All Reservation Menu Components
    private JPanel viewAllPanel;
    private JPanel viewAllBottomPanel;
    private JPanel viewAllTopPanel;
    private JList<String> viewAllTextArea;
    private JButton viewAllBackButton;
    private JScrollPane viewAllScrollPane;
    private JLabel viewAllLabel;
    private JTable viewAllTable;

    // Confirm Delete Res Menu Components
    private JPanel confirmDeleteResPanel;
    private JTextField confirmDeleteEnterPinTextField;
    private JButton confirmDeleteSubmitButton;
    private JLabel confirmDeleteEnterPinLabel;
    private JLabel confirmDeleteLabel;
    private JButton confimDeleteCancelButton;

    ResMenuGUI() {

        // Button actions for the main reservation menu.
        resMenuCreateResButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ResMenuGUI obj = new ResMenuGUI();
                GUIController.updatePanel(obj.getCreateResPanel(), "Create Res");
                GUIController.switchPanel(GUIController.getMainPanel(), "Create Res");
            }
        });

        resMenuModifyResButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ResMenuGUI obj = new ResMenuGUI();
                GUIController.updatePanel(obj.getModifyResPanel(), "Modify Res");
                GUIController.switchPanel(GUIController.getMainPanel(), "Modify Res");
            }
        });

        resMenuDeleteResButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ResMenuGUI obj = new ResMenuGUI();
                GUIController.updatePanel(obj.getDeleteResPanel(), "Delete Res");
                GUIController.switchPanel(GUIController.getMainPanel(), "Delete Res");
            }
        });

        resMenuViewAllButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ResMenuGUI obj = new ResMenuGUI();
                GUIController.updatePanel(obj.getViewAllPanel(), "View All Res");
                GUIController.switchPanel(GUIController.getMainPanel(), "View All Res");
            }
        });

        resMenuBackButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GUIController.switchPanel(GUIController.getMainPanel(), "Main Menu");
            }
        });

        // Button actions for the create reservation menu.
        createBackButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GUIController.switchPanel(GUIController.getMainPanel(), "Reservation Menu");
            }
        });

        createSubmitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                connectDB.setReservation(Integer.parseInt(createTableNumTextField.getText()),
                        createCustNameTextField.getText(), createTimeTextField.getText(),
                        Integer.parseInt(createNumGuestsTextField.getText()), createDateTextField.getText());
                ResMenuGUI obj = new ResMenuGUI();
                GUIController.updatePanel(obj.getViewAllPanel(), "View All Res");
                GUIController.switchPanel(GUIController.getMainPanel(), "View All Res");
            }
        });

        // Button actions for the modify reservation menu.
        modifyBackButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GUIController.switchPanel(GUIController.getMainPanel(), "Reservation Menu");
            }
        });

        modifySubmitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                connectDB.modReservations(Integer.parseInt(modifyTableNumTextField.getText()), modifyCustNameTextField.getText(),
                        modifyResTimeTextField.getText(), Integer.parseInt(modifyNumGuestsTextField.getText()),
                        modifyResDateTextField.getText(), Integer.parseInt(modifyResIdTextField.getText()));
                ResMenuGUI obj = new ResMenuGUI();
                GUIController.updatePanel(obj.getViewAllPanel(), "View All Res");
                GUIController.switchPanel(GUIController.getMainPanel(), "View All Res");
            }
        });

        // Button actions for the delete reservation menu.
        delDeleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String res = String.valueOf(delResSelectComboBox.getSelectedItem());
                ReservationSystem.setDelResId(Integer.parseInt(res.substring(0, 6)));
                int dialogButton = JOptionPane.YES_NO_OPTION;
                int dialogResult = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete this " +
                        "reservation?", "Warning", dialogButton);
                if(dialogResult == JOptionPane.YES_OPTION) {
                    GUIController.switchPanel(GUIController.getMainPanel(), "Confirm Delete Res");
                }
            }
        });
        delCancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GUIController.switchPanel(GUIController.getMainPanel(), "Reservation Menu");
            }
        });

        // Button actions for the confirm delete reservation menu.
        confirmDeleteSubmitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(Integer.parseInt(confirmDeleteEnterPinTextField.getText()) == LogOn.getLoggedInPin()) {
                    connectDB.delReservation(ReservationSystem.getDelResId());
                    GUIController.switchPanel(GUIController.getMainPanel(), "Reservation Menu");
                } else {
                    JOptionPane.showMessageDialog(null, "Invalid pin please try again...");
                    ResMenuGUI obj = new ResMenuGUI();
                    GUIController.updatePanel(obj.getConfirmDeleteResPanel(), "Confirm Delete Res");
                    GUIController.switchPanel(GUIController.getMainPanel(), "Confirm Delete Res");
                }
            }
        });

        confimDeleteCancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GUIController.updatePanel(getDeleteResPanel(), "Delete Res");
                GUIController.switchPanel(GUIController.getMainPanel(), "Delete Res");
            }
        });


        // Button actions for the view all reservations menu.
        viewAllBackButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GUIController.switchPanel(GUIController.getMainPanel(), "Reservation Menu");
            }
        });

        setAddResTable(viewAllTable);

        setAddResTable(modifySelectResTable);

        setResDropDown(delResSelectComboBox);

        modifySelectResTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                int i = modifySelectResTable.getSelectedRow();
                TableModel resTable = modifySelectResTable.getModel();
                modifyResIdTextField.setText(resTable.getValueAt(i,0).toString());
                modifyTableNumTextField.setText(resTable.getValueAt(i,1).toString());
                modifyCustNameTextField.setText(resTable.getValueAt(i,2).toString());
                modifyNumGuestsTextField.setText(resTable.getValueAt(i,3).toString());
                modifyResDateTextField.setText(resTable.getValueAt(i,4).toString());
                modifyResTimeTextField.setText(resTable.getValueAt(i,5).toString());
            }
        });
    }

    private void setAddResTable(JTable inputTable) {
        ReservationSystem.populateReservationsTable(inputTable);
    }

    private void setResDropDown(JComboBox<String> inputBox) {
        ReservationSystem.populateResDropDown(inputBox);
    }


    JPanel getResMenuPanel() {
        return resMenuPanel;
    }

    JPanel getCreateResPanel() {
        return createResPanel;
    }

    JPanel getModifyResPanel() {
        return modifyResPanel;
    }

    JPanel getDeleteResPanel() {
        return deleteResPanel;
    }

    JPanel getViewAllPanel() {
        return viewAllPanel;
    }

    JPanel getConfirmDeleteResPanel() {
        return confirmDeleteResPanel;
    }

}
