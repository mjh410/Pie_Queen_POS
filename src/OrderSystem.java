/**
 * File Name: OrderSystem.java
 * Date: April 06, 2018
 * Author: Matt Huffman and Patricia Sims
 * Course: CMSC 495
 * Assignment: Final Project
 * Purpose: This file handles the order system functions.
 * Created Using: IntelliJ IDEA
 */

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.ArrayList;

class OrderSystem {

    private String itemName;
    private BigDecimal itemPrice;
    private BigDecimal billTotal;
    private int menuId;
    private static int orderCheckId;
    private static String tableNumber;

    static JList populateAddItemList(JList<String> inputList) {

        DefaultListModel<String> listModel = new DefaultListModel<>();
        inputList.setModel(listModel);
        ArrayList<OrderSystem> menuList = connectDB.listMenu();
        String row;
        for(OrderSystem menu : menuList) {
            row = menu.itemName;
            listModel.addElement(row);
        }
        return inputList;
    }

    static JTable populateOrderTable(JTable inputTable, int check) {
        ArrayList<OrderSystem> orderList = connectDB.selectCheck(check);
        String columns[] = {"Item", "Price"};
        DefaultTableModel tableModel = new DefaultTableModel();
        tableModel.setColumnIdentifiers(columns);
        if (orderList == null) {
            inputTable.setModel(tableModel);
            return inputTable;
        }
        Object[] rows = new Object[2];
        for(OrderSystem order : orderList) {
            rows[0] = order.itemName;
            rows[1] = order.itemPrice;
            tableModel.addRow(rows);
        }
        inputTable.setModel(tableModel);
        return inputTable;
    }

    static String[][] convertMyChecksArrayList() {

        ArrayList<String> list = connectDB.listMyChecks(LogOn.getLoggedInPin());
        // Each check has 3 data elements
        String[][] myChecksArray = new String[list.size()/3][3];

        int rowCount = 0;
        int totalElementCount = 0;
        while (rowCount < list.size()/3) {
            while (totalElementCount < list.size()) {
                for (int j = 0; j < 3; j++) {
                    myChecksArray[rowCount][j] = list.get(totalElementCount);
                    totalElementCount++;
                }
                rowCount++;
            }
        }
        return myChecksArray;
    }

    static JList populateMyChecksList(JList<String> inputList, String[][] inputArray) {

        DefaultListModel<String> listModel = new DefaultListModel<>();
        inputList.setModel(listModel);
        String row;
        ResultSet rs;
        String resId = "";
        String name = "";
        String date = "";
        String time = "";
        String table = "";
        String check = "";
        for (int i = 0; i < inputArray.length; i++) {
            rs = connectDB.getReservation(Integer.parseInt(inputArray[i][1]));
            try {
                while(rs.next()) {
                    resId = rs.getString(1);
                    name = rs.getString(2);
                    time = rs.getString(3);
                    date = rs.getString(5);
                    table = rs.getString(6);
                }
            } catch (Exception e) {
                System.err.println("ERROR: No ResultSet data found.");
            }
            check = inputArray[i][0];
            row =  "Check: " + check + "     " + name + "     Reservation: " + resId + "    Table: " + table;
            listModel.addElement(row);
        }
        return inputList;
    }

    public BigDecimal getBillTotal() {
        return billTotal;
    }

    public void setBillTotal(BigDecimal billTotal) {
        this.billTotal = billTotal;
    }

    public BigDecimal getItemPrice() {
        return itemPrice;
    }

    void setItemPrice(BigDecimal itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getItemName() {
        return itemName;
    }

    void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getMenuId() {
        return menuId;
    }

    void setMenuId(int menuId) {
        this.menuId = menuId;
    }

    static int getOrderCheckId() { return orderCheckId; }

    static void setOrderCheckId(int orderCheckId) { OrderSystem.orderCheckId = orderCheckId; }

    public static String getTableNumber() {
        return tableNumber;
    }

    public static void setTableNumber(String tableNumber) {
        OrderSystem.tableNumber = tableNumber;
    }
}
