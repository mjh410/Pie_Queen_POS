/**
 * File Name: LogOn.java
 * Date: April 06, 2018
 * Author: Matt Huffman
 * Course: CMSC 495
 * Assignment: Final Project
 * Purpose: This file handles the log on menu functions.
 * Created Using: IntelliJ IDEA
 */

import javax.swing.*;

class LogOn {

    private static int loggedInEmpId;
    private static int loggedInPin;
    private static String loggedInfName;
    private static String loggedInlName;
    private static String loggedInJobTitle;
    private static String loggedInIsManager;
    private static String loggedInNameTitle;
    private static String loggedInIsEnabled;
    private static String nameTitle;

    boolean verifyLogin(String input) {
        return verifyPinLength(input) && verifyUser(input);
    }

    private boolean verifyUser(String input) {
        if(connectDB.getEmpInfo(Integer.parseInt(input)) == null) {
            JOptionPane.showMessageDialog(null, "Invalid Pin: Please Try Again...");
            return false;
        } else {
            return true;
        }
    }

    private boolean verifyPinLength(String input) {
        if(input.length() == 6) {
            return true;
        } else {
            JOptionPane.showMessageDialog(null, "Invalid Pin: Pin must be 6 digits.");
            return false;
        }
    }

    static int getLoggedInEmpId() {
        return loggedInEmpId;
    }

    static void setLoggedInEmpId(int loggedInEmpId) {
        LogOn.loggedInEmpId = loggedInEmpId;
    }

    static int getLoggedInPin() {
        return loggedInPin;
    }

    static void setLoggedInPin(int loggedInPin) {
        LogOn.loggedInPin = loggedInPin;
    }

    private static String getLoggedInfName() {
        return loggedInfName;
    }

    static void setLoggedInfName(String loggedInfName) {
        LogOn.loggedInfName = loggedInfName;
    }

    private static String getLoggedInlName() {
        return loggedInlName;
    }

    static void setLoggedInlName(String loggedInlName) {
        LogOn.loggedInlName = loggedInlName;
    }

    private static String getLoggedInJobTitle() {
        return loggedInJobTitle;
    }

    static void setLoggedInJobTitle(String loggedInJobTitle) {
        LogOn.loggedInJobTitle = loggedInJobTitle;
    }

    static String getLoggedInIsManager() {
        return loggedInIsManager;
    }

    static void setLoggedInIsManager(String loggedInIsManager) {
        LogOn.loggedInIsManager = loggedInIsManager;
    }

    public static String getLoggedInNameTitle() {
        return loggedInNameTitle;
    }

    public static void setLoggedInNameTitle(String loggedInNameTitle) {
        LogOn.loggedInNameTitle = loggedInNameTitle;
    }

    public static String getLoggedInIsEnabled() {
        return loggedInIsEnabled;
    }

    static void setLoggedInIsEnabled(String loggedInIsEnabled) {
        LogOn.loggedInIsEnabled = loggedInIsEnabled;
    }

    static String getNameTitle() {
        return nameTitle;
    }

    static void setNameTitle() {
        nameTitle = getLoggedInfName() + " " + getLoggedInlName() + " - " + getLoggedInJobTitle();
    }
}