/**
 * File Name: OrderMenuGUI.java
 * Date: April 06, 2018
 * Author: Matt Huffman
 * Course: CMSC 495
 * Assignment: Final Project
 * Purpose: This file contains the code for the order menu GUI.
 * Created Using: IntelliJ IDEA
 */

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;


public class OrderMenuGUI extends JPanel {

    private JPanel mainGUIPanel;

    // Main Order Menu Components.
    private JPanel orderMenuPanel;
    private JButton orderMenuCreateOrderPanel;
    private JList<String> orderMenuMyChecksList;
    private JScrollPane orderMenuOrderScrollPane;
    private JLabel orderMenuOrderLabel;
    private JPanel orderMenuButtonPanel;
    private JButton orderMenuUseSelectedButton;
    private JButton orderMenuCreateNewOrderButton;
    private JLabel orderMenuOptionLabel;
    private JButton orderMenuBackButton;
    private JLabel orderMenuLabel;

    // Create New Order Menu Components.
    private JPanel createNewOrderPanel;
    private JList<String> createNewOrderResList;
    private JScrollPane createNewOrderResScrollPane;
    private JLabel createNewOrderResListLabel;
    private JPanel createNewOrderButtonPanel;
    private JButton createNewOrderUseSelectedResButton;
    private JButton createNewOrderCreateResButton;
    private JLabel createNewOrderOptionLabel;
    private JTextArea createNewOrderNoteTextArea;
    private JButton createNewOrderBackButton;
    private JLabel createNewOrderLabel;

    // Item Menu components.
    private JPanel itemMenuPanel;
    private JPanel itemMenuLeftPanel;
    private JPanel itemMenuRightPanel;
    private JButton itemMenuAddItemsButton;
    private JButton itemMenuRemoveItemsButton;
    private JButton itemMenuPayBillButton;
    private JPanel itemMenuBackbuttonPanel;
    private JPanel itemMenuOrderListPanel;
    private JLabel itemMenuTableLabel;
    private JLabel itemMenuTableNumberLabel;
    private JButton itemMenuBackButton;
    private JScrollPane itemMenuTableOrderScrollPane;
    private JTable itemMenuTableOrderTable;

    // Add Item Menu Components.
    private JPanel addItemPanel;
    private JButton addItemButton;
    private JList<String> addItemList;
    private JScrollPane addItemScrollPane;
    private JButton addItemBackButton;
    private JLabel addItemLabel;

    // Remove Item Menu Components.
    private JPanel removeItemPanel;
    private JButton removeItemButton;
    private JList removeItemList;
    private JScrollPane removeItemScrollPane;
    private JButton removeItemBackButton;
    private JLabel removeItemLabel;

    // Payment Menu Components.
    private JPanel paymentPanel;
    private JTextField paymentCreditCardField;
    private JButton paymentSubmitButton;
    private JLabel paymentCreditCardLabel;
    private JLabel paymentExpDateLabel;
    private JPanel paymentExpPanel;
    private JTextField paymentExpMonthField;
    private JTextField paymentExpYearField;
    private JLabel paymentExpSlashLabel;
    private JTextField paymentCashField;
    private JLabel paymentCashLabel;
    private JLabel paymentOrLabel;
    private JButton paymentCancelButton;
    private JTextField paymentTotalTextField;
    private JLabel paymentTotalLabel;
    private JLabel paymentCheckTotalLabel;
    private JTextField textField1;


    OrderMenuGUI() {

        // Button actions for the main order menu.
        orderMenuCreateNewOrderButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OrderMenuGUI obj = new OrderMenuGUI();
                obj.setViewExistingRes(createNewOrderResList);
                GUIController.updatePanel(obj.getCreateNewOrderPanel(), "Create Order Menu");
                GUIController.switchPanel(GUIController.getMainPanel(), "Create Order Menu");
            }
        });
        orderMenuUseSelectedButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OrderMenuGUI obj = new OrderMenuGUI();
                obj.openOrder(getOrdersStringArray());
                GUIController.updatePanel(obj.getItemMenuPanel(), "Item Menu");
                GUIController.switchPanel(GUIController.getMainPanel(), "Item Menu");
            }
        });
        orderMenuBackButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GUIController.switchPanel(GUIController.getMainPanel(), "Main Menu");
            }
        });

        // Button actions for the create new order menu.
        createNewOrderUseSelectedResButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OrderMenuGUI obj = new OrderMenuGUI();
                obj.createNewOrder(getResStringArray());
                GUIController.updatePanel(obj.getItemMenuPanel(), "Item Menu");
                GUIController.switchPanel(GUIController.getMainPanel(), "Item Menu");
            }
        });
        createNewOrderCreateResButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ResMenuGUI obj = new ResMenuGUI();
                GUIController.updatePanel(obj.getCreateResPanel(), "Create Res");
                GUIController.switchPanel(GUIController.getMainPanel(), "Create Res");
            }
        });
        createNewOrderBackButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GUIController.switchPanel(GUIController.getMainPanel(), "Order Menu");
            }
        });

        // Button actions for item menu.
        itemMenuAddItemsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OrderMenuGUI obj = new OrderMenuGUI();
                GUIController.updatePanel(obj.getAddItemPanel(), "Add Item");
                GUIController.switchPanel(GUIController.getMainPanel(), "Add Item");
            }
        });
        itemMenuRemoveItemsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                removeItem();
                OrderMenuGUI obj = new OrderMenuGUI();
                OrderSystem.populateOrderTable(obj.itemMenuTableOrderTable, OrderSystem.getOrderCheckId());
                obj.itemMenuTableNumberLabel.setText(OrderSystem.getTableNumber());
                GUIController.updatePanel(obj.getItemMenuPanel(), "Item Menu");
                GUIController.switchPanel(GUIController.getMainPanel(), "Item Menu");
            }
        });
        itemMenuPayBillButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OrderMenuGUI obj = new OrderMenuGUI();
                obj.paymentTotalTextField.setText(String.valueOf(getTotal()));
                GUIController.updatePanel(obj.getPaymentPanel(), "Payment");
                GUIController.switchPanel(GUIController.getMainPanel(), "Payment");
            }
        });
        itemMenuBackButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OrderMenuGUI obj = new OrderMenuGUI();
                obj.setMyChecksList();
                GUIController.updatePanel(obj.getOrderMenuPanel(), "Order Menu");
                GUIController.switchPanel(GUIController.getMainPanel(), "Order Menu");
            }
        });

        // Button actions for add item menu.
        addItemBackButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GUIController.switchPanel(GUIController.getMainPanel(), "Item Menu");
            }
        });

        // Button actions for the remove item menu.
        removeItemBackButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GUIController.switchPanel(GUIController.getMainPanel(), "Item Menu");
            }
        });

        // Button actions for the payment menu.
        paymentCancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GUIController.switchPanel(GUIController.getMainPanel(), "Item Menu");
            }
        });

        addItemButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addItem();
                OrderMenuGUI obj = new OrderMenuGUI();
                OrderSystem.populateOrderTable(obj.itemMenuTableOrderTable, OrderSystem.getOrderCheckId());
                obj.itemMenuTableNumberLabel.setText(OrderSystem.getTableNumber());
                GUIController.updatePanel(obj.getItemMenuPanel(), "Item Menu");
                GUIController.switchPanel(GUIController.getMainPanel(), "Item Menu");
            }
        });

        paymentSubmitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (verifyPayment()){
                    submitPayment();
                    GUIController.switchPanel(GUIController.getMainPanel(), "Main Menu");
                }
            }
        });

        setAddItemList(addItemList);
        setViewExistingRes(createNewOrderResList);

    }

    JPanel getItemMenuPanel() {
        return itemMenuPanel;
    }

    JPanel getAddItemPanel() {
        return addItemPanel;
    }

    JPanel getRemoveItemPanel() {
        return removeItemPanel;
    }

    JPanel getPaymentPanel() {
        return paymentPanel;
    }

    JPanel getOrderMenuPanel() {
        return orderMenuPanel;
    }

    JPanel getCreateNewOrderPanel() {
        return createNewOrderPanel;
    }

    private void setAddItemList(JList<String> inputList) {
        OrderSystem.populateAddItemList(inputList);
    }

    private void openOrder(String[] check) {
        OrderSystem.setOrderCheckId(Integer.parseInt(check[1]));
        OrderSystem.setTableNumber(check[6]);
        itemMenuTableNumberLabel.setText(OrderSystem.getTableNumber());
        OrderSystem.populateOrderTable(itemMenuTableOrderTable, OrderSystem.getOrderCheckId());
    }

    private void setViewExistingRes(JList<String> inputList) {
        ReservationSystem.populateReservationsList(inputList);
    }

    void setMyChecksList() {
        String[][] array = OrderSystem.convertMyChecksArrayList();
        OrderSystem.populateMyChecksList(orderMenuMyChecksList, array);
    }

    private String[] getResStringArray() {
        String resString = createNewOrderResList.getSelectedValue();
        return resString.split("\\s+");
    }

    private String[] getOrdersStringArray() {
        String ordersString = orderMenuMyChecksList.getSelectedValue();
        return ordersString.split("\\s+");
    }

    private void createNewOrder(String[] reservation) {
        int resId = Integer.parseInt(reservation[0]);
        int tableId = Integer.parseInt(reservation[1]);
        OrderSystem.setTableNumber(reservation[1]);
        itemMenuTableNumberLabel.setText(OrderSystem.getTableNumber());
        OrderSystem.setOrderCheckId(connectDB.openCheck(tableId, LogOn.getLoggedInEmpId(), resId));
    }

    private void addItem() {
        String item = addItemList.getSelectedValue();
        int menuId = connectDB.getMenuId(item);
        connectDB.addOrder(OrderSystem.getOrderCheckId(), menuId);
    }

    private int getRemoveItemID() {
        int column = 0;
        int row = itemMenuTableOrderTable.getSelectedRow();
        String value = itemMenuTableOrderTable.getModel().getValueAt(row, column).toString();
        return connectDB.getMenuId(value);
    }

    private void removeItem() {
        connectDB.delOrder(OrderSystem.getOrderCheckId(), getRemoveItemID());
    }

    private BigDecimal getTotal() {
        BigDecimal total = connectDB.totalCheck(OrderSystem.getOrderCheckId());
        return total;
    }

    private boolean verifyPayment() {
        String total = paymentTotalTextField.getText();
        String cash = paymentCashField.getText();
        String ccNum = paymentCreditCardField.getText();
        String month = paymentExpMonthField.getText();
        String year = paymentExpYearField.getText();
        if (cash.length() != 0 && ccNum.length() != 0) {
            if ((ccNum.matches("[0-9]+") && month.matches("[0-9]+") && year.matches("[0-9]+")) &&
                    ((ccNum.length() == 16) && (month.length() == 2) && (year.length() == 2))) {
                return true;
            } else {
                JOptionPane.showMessageDialog(null, "Payment Invalid, Please Try Again...");
            }
        } else if (cash.length() != 0) {
            if (total.equals(cash)) {
                return true;
            } else {
                JOptionPane.showMessageDialog(null, "Payment Invalid, Please Try Again...");
            }
        }else if (ccNum.matches("[0-9]+") && month.matches("[0-9]+") && year.matches("[0-9]+")) {
            if ((ccNum.length() == 16) && (month.length() == 2) && (year.length() == 2)) {
                return true;
            } else {
                JOptionPane.showMessageDialog(null, "Payment Invalid, Please Try Again...");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Payment Invalid, Please Try Again...");
        }
        return false;
    }

    private void submitPayment() {
        connectDB.payCheck(OrderSystem.getOrderCheckId());
        JOptionPane.showMessageDialog(null, "Payment Successful");
    }

}