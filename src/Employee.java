/**
 * File Name: Employee.java
 * Date: April 06, 2018
 * Author: Matt Huffman
 * Course: CMSC 495
 * Assignment: Final Project
 * Purpose: This file is the structure for each Employee.
 * Created Using: IntelliJ IDEA
 */

class Employee {

    private int empId;
    private int pin;
    private String fName;
    private String lName;
    private String jobTitle;
    private String isManager;
    private String isEnabled;

    int getEmpId() {
        return empId;
    }

    void setEmpId(int empId) {
        this.empId = empId;
    }

    int getPin() {
        return pin;
    }

    void setPin(int pin) {
        this.pin = pin;
    }

    String getfName() {
        return fName;
    }

    void setfName(String fName) {
        this.fName = fName;
    }

    String getlName() {
        return lName;
    }

    void setlName(String lName) {
        this.lName = lName;
    }

    String getJobTitle() {
        return jobTitle;
    }

    void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    String getIsManager() {
        return isManager;
    }

    void setIsManager(String isManager) {
        this.isManager = isManager;
    }

    String getIsEnabled() {
        return isEnabled;
    }

    void setIsEnabled(String isEnabled) {
        this.isEnabled = isEnabled;
    }
}
