/**
 * File Name: MgmtMenuGUI.java
 * Date: April 06, 2018
 * Author: Matt Huffman and Muhamed Hodzic
 * Course: CMSC 495
 * Assignment: Final Project
 * Purpose: This file contains the code for the management menu GUI.
 * Created Using: IntelliJ IDEA
 */

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class MgmtMenuGUI extends JPanel {

    private JPanel mainGUIPanel;

    // Main Management Menu Components.
    private JPanel mgmtMenuPanel;
    private JButton createUserButton;
    private JButton disableUserButton;
    private JButton reportsButton;
    private JButton mgmtMenuBackButton;

    // Add User Menu Components.
    private JPanel addUserPanel;
    private JLabel firstNameLabel;
    private JLabel jobTitleLabel;
    private JLabel lastNameLabel;
    private JLabel roleLabel;
    private JTextField firstNameTextField;
    private JTextField jobTitleTextField;
    private JTextField lastNameTextField;
    private JTextField passwordTextField;
    private JButton addUserBackButton;
    private JButton addUserSubmitButton;

    // Disable User Menu Components.
    private JPanel disableUserPanel;
    private JButton disableUserSubmitButton;
    private JComboBox<String> selectUserDelete;
    private JButton disableBackButton;
    private JLabel selectUserLabel;

    // Reports Menu Components.
    private JPanel reportsPanel;
    private JButton dailyReportButton;
    private JButton weeklyReportButton;
    private JButton menuReportButton;
    private JButton monthlyReportButton;
    private JButton reportsBackButton;

    // Daily Report Menu Components.
    private JPanel dailySalesPanel;
    private JScrollPane dailyScrollPane;
    private JPanel bottomPanelDailySales;
    private JButton backButtonDailySales;
    private JPanel topPanelDailySales;
    private JTable tableAreaDailySales;
    private JLabel dailyLabel;

    // Weekly Report Menu Components.
    private JPanel weeklySalesPanel;
    private JScrollPane weeklyScrollPane;
    private JPanel bottomPanelWeeklySales;
    private JButton backButtonWeeklySales;
    private JPanel topPanelWeeklySales;
    private JTable tableAreaWeeklySales;
    private JLabel weeklyLabel;


    // Monthly Report Menu Components.
    private JPanel monthlySalesPanel;
    private JScrollPane monthlyScrollPane;
    private JPanel bottomPanelMonthlySales;
    private JButton backButtonMonthlySales;
    private JPanel topPanelMonthlySales;
    private JTable tableAreaMonthlySales;
    private JLabel monthlyLabel;

    // Menu Report Menu Components
    private JPanel menuReportPanel;
    private JScrollPane menuReportScrollPane;
    private JPanel bottomPanelMenuReport;
    private JButton backButtonMenuReport;
    private JPanel topPanelMenuReport;
    private JLabel menuReportLabel;
    private JTable tableAreaMenuReport;
    private JCheckBox managerCheckBox;
    private JLabel managerCheckBoxLabel;
    private JPanel confirmDisableUserPanel;
    private JTextField confirmDisableEnterPinTextField;
    private JButton confirmDisableSubmitButton;
    private JButton confirmDisableCancelButton;
    private JLabel confirmDisableLabel;
    private JLabel confirmDisableEnterPinLabel;


    MgmtMenuGUI() {

        // Buttons actions for the Main Management Menu.
        createUserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MgmtMenuGUI obj = new MgmtMenuGUI();
                GUIController.updatePanel(obj.getAddUserPanel(), "Mgmt Add User");
                GUIController.switchPanel(GUIController.getMainPanel(), "Mgmt Add User");
            }
        });
        disableUserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MgmtMenuGUI obj = new MgmtMenuGUI();
                GUIController.updatePanel(obj.getDisableUserPanel(), "Mgmt Disable User");
                GUIController.switchPanel(GUIController.getMainPanel(), "Mgmt Disable User");
            }
        });
        reportsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GUIController.switchPanel(GUIController.getMainPanel(), "Reports Menu");
            }
        });
        mgmtMenuBackButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GUIController.switchPanel(GUIController.getMainPanel(), "Main Menu");
            }
        });
        addUserSubmitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addUser();
                GUIController.switchPanel(GUIController.getMainPanel(), "Management Menu");
            }
        });

        // Button actions for the add user menu.
        addUserBackButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MgmtMenuGUI obj = new MgmtMenuGUI();
                GUIController.switchPanel(GUIController.getMainPanel(), "Management Menu");
            }
        });

        disableUserSubmitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String user = String.valueOf(selectUserDelete.getSelectedItem());
                ManagementSystem.setDisUserId(Integer.parseInt(user.substring(0, 6)));
                int dialogButton = JOptionPane.YES_NO_OPTION;
                int dialogResult = JOptionPane.showConfirmDialog(null, "Are you sure you want to disable this " +
                        "user?", "Warning", dialogButton);
                if(dialogResult == JOptionPane.YES_OPTION) {
                    GUIController.switchPanel(GUIController.getMainPanel(), "Confirm Disable User");
                } else {
                    GUIController.switchPanel(GUIController.getMainPanel(), "Management Menu");
                }
            }
        });

        // Button actions for the disable user menu.
        disableBackButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GUIController.switchPanel(GUIController.getMainPanel(), "Management Menu");
            }
        });

        confirmDisableSubmitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int input = Integer.parseInt(confirmDisableEnterPinTextField.getText());
                int pin = LogOn.getLoggedInPin();
                if(input == pin) {
                    connectDB.disableEmployee(ManagementSystem.getDisUserId());
                    JOptionPane.showMessageDialog(null, "User has been disabled.");
                    GUIController.switchPanel(GUIController.getMainPanel(), "Management Menu");
                } else {
                    JOptionPane.showMessageDialog(null, "Invalid pin please try again...");
                    MgmtMenuGUI obj = new MgmtMenuGUI();
                    GUIController.updatePanel(obj.getConfirmDisableUserPanel(), "Confirm Disable User");
                    GUIController.switchPanel(GUIController.getMainPanel(), "Confirm Disable User");
                }
            }
        });

        confirmDisableCancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GUIController.updatePanel(getDisableUserPanel(), "Disable User");
                GUIController.switchPanel(GUIController.getMainPanel(), "Disable User");
            }
        });

        // Button actions for the reports menu.
        dailyReportButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MgmtMenuGUI obj = new MgmtMenuGUI();
                GUIController.updatePanel(obj.getDailySalesPanel(), "Daily Report");
                GUIController.switchPanel(GUIController.getMainPanel(), "Daily Report");
            }
        });
        weeklyReportButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MgmtMenuGUI obj = new MgmtMenuGUI();
                GUIController.updatePanel(obj.getWeeklySalesPanel(), "Weekly Report");
                GUIController.switchPanel(GUIController.getMainPanel(), "Weekly Report");
            }
        });
        monthlyReportButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MgmtMenuGUI obj = new MgmtMenuGUI();
                GUIController.updatePanel(obj.getMonthlySalesPanel(), "Monthly Report");
                GUIController.switchPanel(GUIController.getMainPanel(), "Monthly Report");
            }
        });
        menuReportButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MgmtMenuGUI obj = new MgmtMenuGUI();
                GUIController.updatePanel(obj.getMenuReportPanel(), "Menu Report");
                GUIController.switchPanel(GUIController.getMainPanel(), "Menu Report");
            }
        });
        reportsBackButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GUIController.switchPanel(GUIController.getMainPanel(), "Management Menu");
            }
        });

        // Button actions for daily report menu.
        backButtonDailySales.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GUIController.switchPanel(GUIController.getMainPanel(), "Reports Menu");
            }
        });

        // Button actions for weekly report menu.
        backButtonWeeklySales.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GUIController.switchPanel(GUIController.getMainPanel(), "Reports Menu");
            }
        });

        // Button actions for monthly report menu.
        backButtonMonthlySales.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GUIController.switchPanel(GUIController.getMainPanel(), "Reports Menu");
            }
        });

        // Button actions for menu report menu.
        backButtonMenuReport.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GUIController.switchPanel(GUIController.getMainPanel(), "Reports Menu");
            }
        });

        setDailySalesReport(tableAreaDailySales);

        setWeeklySalesReport(tableAreaWeeklySales);

        setMonthlySalesReport(tableAreaMonthlySales);

        setMenuReport(tableAreaMenuReport);

        fillDisableEmpDropDown(selectUserDelete);
    }

    private void addUser() {
        String mgr = "0";
        if (managerCheckBox.isSelected()) {
            mgr = "1";
        }
        connectDB.addEmployee(Integer.parseInt(passwordTextField.getText()),
                firstNameTextField.getText(), lastNameTextField.getText(),
                jobTitleTextField.getText(), mgr);
        JOptionPane.showMessageDialog(null, firstNameTextField.getText() + " "
                + lastNameTextField.getText() +" has been created.");
    }
    private void setDailySalesReport(JTable dailyTable) {
        ManagementSystem.populateDailyReport(dailyTable);
    }

    private void setWeeklySalesReport(JTable weeklyTable) {
        ManagementSystem.populateWeeklyReport(weeklyTable);
    }

    private void setMonthlySalesReport(JTable monthlyTable) {
        ManagementSystem.populateMonthlyReport(monthlyTable);
    }

    private void setMenuReport(JTable menuTable) {
        ManagementSystem.populateMenuReport(menuTable);
    }

    JPanel getMgmtMenuPanel() {
        return mgmtMenuPanel;
    }

    JPanel getAddUserPanel() {
        return addUserPanel;
    }

    JPanel getDisableUserPanel() {
        return disableUserPanel;
    }

    JPanel getReportsPanel() {
        return reportsPanel;
    }

    JPanel getDailySalesPanel() {
        return dailySalesPanel;
    }

    JPanel getWeeklySalesPanel() {
        return weeklySalesPanel;
    }

    JPanel getMonthlySalesPanel() {
        return monthlySalesPanel;
    }

    JPanel getMenuReportPanel() {
        return menuReportPanel;
    }

    JPanel getConfirmDisableUserPanel() {
        return confirmDisableUserPanel;
    }

    private static JComboBox fillDisableEmpDropDown(JComboBox<String> inputBox) {
        ArrayList<Employee> empList = connectDB.listEmployees();
        for(Employee emp : empList) {
            if(emp.getIsEnabled().equals("1")) {
                inputBox.addItem(Integer.toString(emp.getEmpId()) + " - " +
                        emp.getfName() + " " + emp.getlName());
            }
        }
        return inputBox;
    }
}