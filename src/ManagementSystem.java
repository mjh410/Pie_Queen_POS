/**
 * File Name: ManagementSystem.java
 * Date: April 06, 2018
 * Author: Patricia Sims
 * Course: CMSC 495
 * Assignment: Final Project
 * Purpose: This file handles the management system functions.
 * Created Using: IntelliJ IDEA
 */

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;

class ManagementSystem {

    private static int disUserId;

    static JTable populateDailyReport(JTable inputTable) {
        ArrayList<Report> dailyList = connectDB.dailyReport();
        String columns[] = {"Employee ID", "First Name", "Last Name", "Total", "Table", "Guests"};
        DefaultTableModel tableModel = new DefaultTableModel();
        tableModel.setColumnIdentifiers(columns);
        if (dailyList == null) {
            inputTable.setModel(tableModel);
            return inputTable;
        }
        Object[] rows = new Object[6];
        for(Report list : dailyList) {
            rows[0] = Integer.toString(list.empId);
            rows[1] = list.fName;
            rows[2] = list.lName;
            rows[3] = String.valueOf(list.total);
            rows[4] = Integer.toString(list.table);
            rows[5] = Integer.toString(list.numOfGuests);
            tableModel.addRow(rows);
        }
        inputTable.setModel(tableModel);
        return inputTable;
    }

    static JTable populateWeeklyReport(JTable inputTable) {
        ArrayList<Report> weeklyList = connectDB.weeklyReport();
        String columns[] = {"Employee ID", "First Name", "Last Name", "Total", "Table", "Guests"};
        DefaultTableModel tableModel = new DefaultTableModel();
        tableModel.setColumnIdentifiers(columns);
        if (weeklyList == null) {
            inputTable.setModel(tableModel);
            return inputTable;
        }
        Object[] rows = new Object[6];
        for(Report list : weeklyList) {
            rows[0] = Integer.toString(list.empId);
            rows[1] = list.fName;
            rows[2] = list.lName;
            rows[3] = String.valueOf(list.total);
            rows[4] = Integer.toString(list.table);
            rows[5] = Integer.toString(list.numOfGuests);
            tableModel.addRow(rows);
        }
        inputTable.setModel(tableModel);
        return inputTable;
    }

    static JTable populateMonthlyReport(JTable inputTable) {
        ArrayList<Report> monthlyList = connectDB.monthlyReport();
        String columns[] = {"Employee ID", "First Name", "Last Name", "Total", "Table", "Guests"};
        DefaultTableModel tableModel = new DefaultTableModel();
        tableModel.setColumnIdentifiers(columns);
        if (monthlyList == null) {
            inputTable.setModel(tableModel);
            return inputTable;
        }
        Object[] rows = new Object[6];
        for(Report list : monthlyList) {
            rows[0] = Integer.toString(list.empId);
            rows[1] = list.fName;
            rows[2] = list.lName;
            rows[3] = String.valueOf(list.total);
            rows[4] = Integer.toString(list.table);
            rows[5] = Integer.toString(list.numOfGuests);
            tableModel.addRow(rows);
        }
        inputTable.setModel(tableModel);
        return inputTable;
    }

    static JTable populateMenuReport(JTable inputTable) {
        ArrayList<Report> menuList = connectDB.menuReport();
        String columns[] = {"Menu Item" , "Number Sold" , "Total of Sales"};
        DefaultTableModel tableModel = new DefaultTableModel();
        tableModel.setColumnIdentifiers(columns);
        if (menuList == null) {
            inputTable.setModel(tableModel);
            return inputTable;
        }
        Object[] rows = new Object[3];
        for (Report list : menuList) {
            rows[0] = list.item;
            rows[1] = Integer.toString(list.count);
            rows[2] = String.valueOf(list.sum);
            tableModel.addRow(rows);
        }
        inputTable.setModel(tableModel);
        return inputTable;
    }

    static int getDisUserId() {
        return disUserId;
    }

    static void setDisUserId(int disUserId) {
        ManagementSystem.disUserId = disUserId;
    }
}
