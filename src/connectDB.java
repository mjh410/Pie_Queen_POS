/**
 * File Name: connectDB.java
 * Date: April 15, 2018
 * Author: Patricia Sims
 * Course: CMSC 495
 * Assignment: Final Project
 * Purpose: This file is the database JDBC connection class.
 * Created Using: IntelliJ IDEA
 */

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;

public class connectDB {

    private Config cfg = new Config();
    private String schema = cfg.getProperty("DBSchema");
    private String password = cfg.getProperty("DBPassword");
    private String url = cfg.getProperty("DBUrl");
    private String driver = cfg.getProperty("DBDriver");
    private static Connection connect;
    private static PreparedStatement stmt;
    private static ResultSet rs;

    Connection initializeConnection() {
        try {
            Class.forName(driver).newInstance();
            connect = DriverManager.getConnection(url, schema, password);
            System.out.println("Mysql connection established");
        } catch (Exception e) {
            System.err.println("ERROR: Cannot connect to the mysql server");
        }
        return connect;
    }

    public static ArrayList<Employee> listEmployees()  {
        //Returns an array of LogOn objects with the following values from the employees table
        //empId, #pin, fName, lName, jobTitle, isManager, #isEnabled
        String sqlListEmployees = "select * from employees;";
        ArrayList<Employee> empList = new ArrayList<>();
        try {
            stmt = connect.prepareStatement(sqlListEmployees);
            rs = stmt.executeQuery();
            while (rs.next()) {
                Employee emp = new Employee();
                emp.setEmpId(rs.getInt("employee_id"));
                emp.setPin(rs.getInt("pin"));
                emp.setfName(rs.getString("first_name"));
                emp.setlName(rs.getString("last_name"));
                emp.setJobTitle(rs.getString("job_title"));
                emp.setIsManager(rs.getString("is_manager"));
                emp.setIsEnabled(rs.getString("is_enabled"));
                empList.add(emp);
            }
        } catch (Exception e) {
            e.printStackTrace();
            empList = null;
        }
        return empList;
    }

    static ArrayList<Employee> getEmpInfo(int pin) {
        //Returns an array of LogOn objects of the employee specified by pin
        //empId, #pin, fName, lName, jobTitle, isManager, #isEnabled
        String sqlGetEmpInfo = "select * from employees where pin = ?";
        ArrayList<Employee> empInfo = new ArrayList<>();

        try {
            stmt = connect.prepareStatement(sqlGetEmpInfo);
            stmt.setInt(1, pin);
            rs = stmt.executeQuery();
            if(rs.next() == false) {
                return null;
            } else {
                Employee emp = new Employee();
                emp.setEmpId(rs.getInt("employee_id"));
                LogOn.setLoggedInEmpId(rs.getInt("employee_id"));
                emp.setPin(rs.getInt("pin"));
                LogOn.setLoggedInPin(rs.getInt("pin"));
                emp.setfName(rs.getString("first_name"));
                LogOn.setLoggedInfName(rs.getString("first_name"));
                emp.setlName(rs.getString("last_name"));
                LogOn.setLoggedInlName(rs.getString("last_name"));
                emp.setJobTitle(rs.getString("job_title"));
                LogOn.setLoggedInJobTitle(rs.getString("job_title"));
                emp.setIsManager(rs.getString("is_manager"));
                LogOn.setLoggedInIsManager(rs.getString("is_manager"));
                emp.setIsEnabled(rs.getString("is_enabled"));
                LogOn.setLoggedInIsEnabled(rs.getString("is_enabled"));
                LogOn.setNameTitle();
                empInfo.add(emp);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return empInfo;
    }

    public static void addEmployee(int pin, String fName, String lName, String job, String mgr) {
        String sqlAddEmployee = "insert into employees (pin,first_name,last_name,job_title,is_manager,is_enabled) " +
                "values  (?, ?, ?, ?, ?, '1')";
        try {
            stmt = connect.prepareStatement(sqlAddEmployee);
            stmt.setInt(1, pin);
            stmt.setString(2, fName);
            stmt.setString(3, lName);
            stmt.setString(4, job);
            stmt.setString(5, mgr);
            stmt.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void disableEmployee(int empId) {
        String sqlDisableEmp = "update employees set is_enabled = '0' where employee_id = ?";
        try {
            stmt = connect.prepareStatement(sqlDisableEmp);
            stmt.setInt(1, empId);
            stmt.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static ArrayList<ReservationSystem> listReservations()  {
        //Returns an arraylist of ReservationSystem type with the following values:
        //resId, tableNumber, name, numbOfGuests, date (YYYY-MM-DD), time (##:##)
        String sqlListReservations = "select * from reservation_list;";
        ArrayList<ReservationSystem> resList = new ArrayList<>();
        try {
            stmt = connect.prepareStatement(sqlListReservations);
            rs = stmt.executeQuery();
            while (rs.next()) {
                ReservationSystem reservation = new ReservationSystem();
                reservation.setResId(rs.getInt("reservation_id"));
                reservation.setTableNumber(rs.getInt("table_num"));
                reservation.setName(rs.getString("guest_name"));
                reservation.setNumOfGuests(rs.getInt("number_guests"));
                reservation.setDate(rs.getString("res_date"));
                reservation.setTime(rs.getString("res_time"));
                reservation.setVisible(rs.getString("is_visible"));
                resList.add(reservation);
            }
        } catch (Exception e) {
            e.printStackTrace();
            resList = null;
        }
        return resList;
    }

    public static void setReservation(int table, String guestName, String resTime, int numGuests, String resDate) {
        String sqlSetReservation = "insert into reservations(table_id,guest_name,res_time,number_guests,res_date) " +
                "values((select table_id from tables where table_num = ?),?,?,?,?);";
        try {
            stmt = connect.prepareStatement(sqlSetReservation);
            stmt.setInt(1, table);
            stmt.setString(2, guestName);
            stmt.setString(3, resTime);
            stmt.setInt(4, numGuests);
            stmt.setString(5, resDate);
            stmt.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ResultSet getReservation(int resId) {
        String sqlGetReservation = "select * from reservations where reservation_id = ?";
        try {
            stmt = connect.prepareStatement(sqlGetReservation);
            stmt.setInt(1, resId);
            rs = stmt.executeQuery();
        } catch (Exception e) {
            e.printStackTrace();
            rs = null;
        }
        return rs;
    }

    public static void modReservations(int table, String name, String resTime, int numGuests, String resDate, int resId ) {
        String sqlModReservation = "update reservations set table_id = ?, guest_name = ?, res_time = ?, " +
                "number_guests = ?, res_date = ? where reservation_id = ?";
        try {
            stmt = connect.prepareStatement(sqlModReservation);
            stmt.setInt(1, table);
            stmt.setString(2, name);
            stmt.setString(3, resTime);
            stmt.setInt(4,numGuests);
            stmt.setString(5, resDate);
            stmt.setInt(6, resId);
            stmt.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static void delReservation(int resId) {
        String sqlDelReservation = "delete from reservations where reservation_id = ?";
        try {
            stmt = connect.prepareStatement(sqlDelReservation);
            stmt.setInt(1,resId);
            stmt.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static ArrayList<OrderSystem> listMenu() {
        //Returns an array of Strings of the menu items with the following values:
        //menu_id and item_name
        String sqlMenuList = "select menu_id, item_name, item_price from menu;";
        ArrayList<OrderSystem> menuList = new ArrayList<>();
        try {
            stmt = connect.prepareStatement(sqlMenuList);
            rs = stmt.executeQuery();
            while (rs.next()) {
                OrderSystem menu = new OrderSystem();
                menu.setMenuId(rs.getInt("menu_id"));
                menu.setItemName(rs.getString("item_name"));
                menu.setItemPrice(rs.getBigDecimal(("item_price")));
                menuList.add(menu);
            }
        } catch (Exception e) {
            e.printStackTrace();
            menuList = null;
        }
        return menuList;
    }

    public static ArrayList<String> listAllChecks()  {
        //Returns an array of Strings of all open checks with the following values:
        //pin, check_id, reservation_id, table_num
        String sqlAllChecks = "select * from all_checks_list;";
        ArrayList<String> allChecksList = new ArrayList<String>();
        try {
            stmt = connect.prepareStatement(sqlAllChecks);
            rs = stmt.executeQuery();
            while (rs.next()) {
                allChecksList.add(Integer.toString(rs.getInt("pin")));
                allChecksList.add(Integer.toString(rs.getInt("check_id")));
                allChecksList.add(Integer.toString(rs.getInt("reservation_id")));
                allChecksList.add(Integer.toString(rs.getInt("table_num")));
            }
        } catch (Exception e) {
            e.printStackTrace();
            allChecksList = null;
        }
        return allChecksList;
    }

    public static ArrayList<String> listMyChecks(int pin)  {
        //Returns an array of Strings of all open checks with the following values:
        //check_id, reservation_id, table_num
        String sqlAllChecks = "select * from all_checks_list where pin = ?";
        ArrayList<String> myChecksList = new ArrayList<String>();
        try {
            stmt = connect.prepareStatement(sqlAllChecks);
            stmt.setInt(1, pin);
            rs = stmt.executeQuery();
            while (rs.next()) {
                myChecksList.add(Integer.toString(rs.getInt("check_id")));
                myChecksList.add(Integer.toString(rs.getInt("reservation_id")));
                myChecksList.add(Integer.toString(rs.getInt("table_num")));
            }
        } catch (Exception e) {
            e.printStackTrace();
            myChecksList = null;
        }
        return myChecksList;
    }

    public static int openCheck(int table, int empId, int resId) {
        String sqlOpenCheck = "insert into checks (reservation_id,employee_id,is_paid," +
                "res_time,res_date,table_id) values (?,?,'0',(select res_time from " +
                "reservations where reservation_id = ?), (select res_date from reservations " +
                "where reservation_id = ?),(select table_id from tables where table_num = ?))";
        String sqlUpdateRes = "update reservations set is_visible = '0' where reservation_id = ?";
        String sqlGetCheckId = "select check_id from checks where reservation_id = ? and " +
                "table_id = (select table_num from tables where table_id = ?) and employee_id = ?";
        int checkId = 0;
        try {
            stmt = connect.prepareStatement(sqlOpenCheck);
            stmt.setInt(1, resId);
            stmt.setInt(2, empId);
            stmt.setInt(3, resId);
            stmt.setInt(4, resId);
            stmt.setInt(5, table);
            stmt.executeUpdate();
            stmt = connect.prepareStatement(sqlUpdateRes);
            stmt.setInt(1,resId);
            stmt.executeUpdate();
            stmt = connect.prepareStatement(sqlGetCheckId);
            stmt.setInt(1, resId);
            stmt.setInt(2, table);
            stmt.setInt(3, empId);
            rs = stmt.executeQuery();
            rs.next();
            checkId= rs.getInt("check_id");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return checkId;
    }

    public static ArrayList<OrderSystem> selectCheck(int check) {
        String sqlSelectCheck = "select item_name, item_price from order_list where check_id = ?";
        ArrayList<OrderSystem> orderList = new ArrayList<>();
        try {
            stmt = connect.prepareStatement(sqlSelectCheck);
            stmt.setInt(1, check);
            rs = stmt.executeQuery();
            while (rs.next()) {
                OrderSystem menu = new OrderSystem();
                menu.setItemName(rs.getString("item_name"));
                menu.setItemPrice(rs.getBigDecimal("item_price"));
                orderList.add(menu);
            }
        } catch (Exception e) {
            e.printStackTrace();
            orderList = null;
        }
        return orderList;
    }


    public static void addOrder(int check, int item) {
        String sqlAddOrder = "insert into orders (check_id, menu_id, item_price) " +
                "values (?, ?,(select item_price from menu where menu_id = ?))";
        String sqlUpdateTotal = "update checks set total = total + (select item_price from menu where menu_id = ?) " +
                "where check_id = ?";
        try {
            stmt = connect.prepareStatement(sqlAddOrder);
            stmt.setInt(1, check);
            stmt.setInt(2, item);
            stmt.setInt(3, item);
            stmt.executeUpdate();
            stmt = connect.prepareStatement(sqlUpdateTotal);
            stmt.setInt(1, item);
            stmt.setInt(2, check);
            stmt.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void delOrder(int check, int item) {
        String sqlDelOrder = "delete from orders where check_id = ? and menu_id  = ? limit 1";
        try {
            stmt = connect.prepareStatement(sqlDelOrder);
            stmt.setInt(1, check);
            stmt.setInt(2, item);
            stmt.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int getMenuId(String item) {
        String sqlGetMenuId = "select menu_id from menu where item_name = ?";;
        int menuId = 0;
        try {
            stmt = connect.prepareStatement(sqlGetMenuId);
            stmt.setString(1, item);
            rs = stmt.executeQuery();
            rs.next();
            menuId = rs.getInt("menu_id");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return menuId;
    }

    public static BigDecimal totalCheck (int check) {
        String sqlTotalCheck = "select sum(item_price) as total from order_list where check_id = ?";
        BigDecimal total;
        try {
            stmt = connect.prepareStatement(sqlTotalCheck);
            stmt.setInt(1, check);
            rs = stmt.executeQuery();
            rs.next();
            total = rs.getBigDecimal("total");
        } catch (Exception e) {
            e.printStackTrace();
            total = null;
        }
        return total;
    }

    public static void payCheck(int check) {
        String sqlPayCheck = "update checks set is_paid = '1', time_closed = sysdate() where check_id = ?";
        try {
            stmt = connect.prepareStatement(sqlPayCheck);
            stmt.setInt(1, check);
            stmt.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Report> dailyReport() {
        //Returns an array of Strings of daily sales report with the following values:
        //employee_id, first_name, last_name, total, table_id, number_guests
        String sqlDailyReport = "select * from daily_sales_report;";
        ArrayList<Report> dailyList = new ArrayList<>();
        try {
            stmt = connect.prepareStatement(sqlDailyReport);
            rs = stmt.executeQuery();
            while (rs.next()) {
                Report report = new Report();
                report.setEmpId(rs.getInt("employee_id"));
                report.setfName(rs.getString("first_name"));
                report.setlName(rs.getString("last_name"));
                report.setTotal(rs.getBigDecimal("total"));
                report.setTable(rs.getInt("table_id"));
                report.setNumOfGuests(rs.getInt("number_guests"));
                dailyList.add(report);
            }
        } catch (Exception e) {
            e.printStackTrace();
            dailyList = null;
        }
        return dailyList;
    }

    public static ArrayList<Report> weeklyReport() {
        //Returns an array of Strings of weekly sales report with the following values:
        //employee_id, first_name, last_name, total, table_id, number_guests
        String sqlWeeklyReport = "select * from weekly_sales_report;";
        ArrayList<Report> weeklyList = new ArrayList<>();
        try {
            stmt = connect.prepareStatement(sqlWeeklyReport);
            rs = stmt.executeQuery();
            while (rs.next()) {
                Report report = new Report();
                report.setEmpId(rs.getInt("employee_id"));
                report.setfName(rs.getString("first_name"));
                report.setlName(rs.getString("last_name"));
                report.setTotal(rs.getBigDecimal("total"));
                report.setTable(rs.getInt("table_id"));
                report.setNumOfGuests(rs.getInt("number_guests"));
                weeklyList.add(report);            }
        } catch (Exception e) {
            e.printStackTrace();
            weeklyList = null;
        }
        return weeklyList;
    }

    public static ArrayList<Report> monthlyReport() {
        //Returns an array of Strings of monthly sales report with the following values:
        //employee_id, first_name, last_name, total, table_id, number_guests
        String sqlMonthlyReport = "select * from monthly_sales_report;";
        ArrayList<Report> monthlyList = new ArrayList<>();
        try {
            stmt = connect.prepareStatement(sqlMonthlyReport);
            rs = stmt.executeQuery();
            while (rs.next()) {
                Report report = new Report();
                report.setEmpId(rs.getInt("employee_id"));
                report.setfName(rs.getString("first_name"));
                report.setlName(rs.getString("last_name"));
                report.setTotal(rs.getBigDecimal("total"));
                report.setTable(rs.getInt("table_id"));
                report.setNumOfGuests(rs.getInt("number_guests"));
                monthlyList.add(report);            }
        } catch (Exception e) {
            e.printStackTrace();
            monthlyList = null;
        }
        return monthlyList;
    }

    public static ArrayList<Report> menuReport() {
        //Returns an array of Strings of the menu report with the following values:
        //item_name, count, sum
        String sqlMenuReport = "select * from menu_report;";
        ArrayList<Report> menuList = new ArrayList<>();
        try {
            stmt = connect.prepareStatement(sqlMenuReport);
            rs = stmt.executeQuery();
            while (rs.next()) {
                Report report = new Report();
                report.setItem(rs.getString("item_name"));
                report.setCount(rs.getInt("number_sold"));
                report.setSum(rs.getBigDecimal("total_sales"));
                menuList.add(report);
            }
        } catch (Exception e) {
            e.printStackTrace();
            menuList = null;
        }
        return menuList;
    }

}

