import java.math.BigDecimal;

/**
 * File Name: Config.java
 * Date: April 28, 2018
 * Author: Patricia Sims
 * Course: CMSC 495
 * Assignment: Final Project
 * Purpose: This file is the structure for each report.
 * Created Using: IntelliJ IDEA
 */

public class Report {

    int empId;
    String fName;
    String lName;
    BigDecimal total;
    int table;
    int numOfGuests;
    String item;
    int count;
    BigDecimal sum;

    public int getEmpId() {
        return empId;
    }

    void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getfName() {
        return fName;
    }

    void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    void setlName(String lName) {
        this.lName = lName;
    }

    public BigDecimal getTotal() {
        return total;
    }

    void setTotal(BigDecimal total) {
        this.total = total;
    }

    public int getTable() {
        return table;
    }

    void setTable(int table) {
        this.table = table;
    }

    public int getNumOfGuests() {
        return numOfGuests;
    }

    void setNumOfGuests(int numOfGuests) {
        this.numOfGuests = numOfGuests;
    }

    public String getItem() { return item; }

    void setItem(String item) { this.item = item; }

    public int getCount() { return count; }

    void setCount(int count) { this.count = count; }

    public BigDecimal getSum() { return sum; }

    public void setSum(BigDecimal sum) { this.sum = sum; }

}
