/**
 * File Name: MainMenuGUI.java
 * Date: April 13, 2018
 * Author: Matt Huffman
 * Course: CMSC 495
 * Assignment: Final Project
 * Purpose: This class handles the code for the main menu GUI.
 * Created Using: IntelliJ IDEA
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainMenuGUI extends JPanel {

    private JPanel mainPanel;
    private JPanel mainMenuRightPanel;
    private JPanel mainMenuUserNamePanel;
    private JLabel mainMenuUserLabel;
    private JLabel mainMenuUserDisplayLabel;
    private JButton mainMenuLogOutButton;
    private JPanel mainMenuLeftPanel;
    private JButton mainMenuReservationsButton;
    private JButton mainMenuOrdersButton;
    private JButton mainMenuManagementButton;
    private JPanel mainMenuPanel;


    MainMenuGUI() {
        mainMenuOrdersButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OrderMenuGUI obj = new OrderMenuGUI();
                obj.setMyChecksList();
                GUIController.updatePanel(obj.getOrderMenuPanel(), "Order Menu");
                GUIController.switchPanel(GUIController.getMainPanel(), "Order Menu");
            }
        });
        mainMenuReservationsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GUIController.switchPanel(GUIController.getMainPanel(), "Reservation Menu");
            }
        });
        mainMenuManagementButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GUIController.switchPanel(GUIController.getMainPanel(), "Management Menu");
            }
        });
        mainMenuLogOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Dispose of all open windows.
                System.gc();
                for (Window window : Window.getWindows()) {
                    window.dispose();
                }
                // Launch the GUI again for new log on.
                GUIController run = new GUIController();
            }
        });
    }

    JPanel getMainPanel() {
        return mainPanel;
    }

    void setUserFieldLabelText(String input) {
        mainMenuUserDisplayLabel.setText(input);
    }

    void disableManagementButton() {
        mainMenuManagementButton.setEnabled(false);
    }
}