/**
 * File Name: Config.java
 * Date: April 28, 2018
 * Author: Patricia Sims
 * Course: CMSC 495
 * Assignment: Final Project
 * Purpose: This file is the configuration class that reads the JDBC connection information from a properties file.
 * Created Using: IntelliJ IDEA
 */

import java.util.Properties;

public class Config {

    Properties cfgFile;

    public Config() {
        cfgFile = new Properties();
        try {
            cfgFile.load(this.getClass().getClassLoader().getResourceAsStream("PieQueen.cfg"));
            } catch (Exception e) {
            e.printStackTrace();;
        }
    }

    public String getProperty(String prop){
        String value = this.cfgFile.getProperty(prop);
        return value;
    }

}
