/**
 * File Name: LogOnGUI.java
 * Date: April 13, 2018
 * Author: Matt Huffman
 * Course: CMSC 495
 * Assignment: Final Project
 * Purpose: This class handles the code for the log on GUI.
 * Created Using: IntelliJ IDEA
 */

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LogOnGUI {
    private JPanel mainPanel;
    private JPanel logOnPanel;
    private JTextField logOnEnterPinTextField;
    private JLabel logOnEnterPinLabel;
    private JButton logOnSubmitButton;
    private JTextArea tempForProfessor;

    JPanel getMainPanel() {
        return mainPanel;
    }

    LogOnGUI() {
        logOnSubmitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LogOn obj = new LogOn();
                MainMenuGUI obj2 = new MainMenuGUI();
                //TODO validate input is a number and not empty.
                if(obj.verifyLogin(logOnEnterPinTextField.getText())) {
                    obj2.setUserFieldLabelText(LogOn.getNameTitle());
                    if(Integer.parseInt(LogOn.getLoggedInIsManager()) == 0) {
                        obj2.disableManagementButton();
                    }
                    GUIController.updatePanel(obj2.getMainPanel(), "Main Menu");
                    GUIController.switchPanel(GUIController.getMainPanel(), "Main Menu");
                }
            }
        });
    }
}