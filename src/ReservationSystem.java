/**
 * File Name: ReservationSystem.java
 * Date: April 06, 2018
 * Author: Trish Sims and Matt Huffman
 * Course: CMSC 495
 * Assignment: Final Project
 * Purpose: This file handles the reservation system functions.
 * Created Using: IntelliJ IDEA
 */

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;

class ReservationSystem {

    private int resId;
    private String name;
    private String time;
    private String date;
    private int tableNumber;
    private int numOfGuests;
    private String visible;
    private static int delResId;

    static JList populateReservationsList(JList<String> inputList) {
        javax.swing.DefaultListModel<String> listModel = new javax.swing.DefaultListModel<>();
        inputList.setModel(listModel);
        ArrayList<ReservationSystem> resList = connectDB.listReservations();
        String heading = String.format("%-25s%-10s%-13s%-10s%-30s%s","Reservation Number","Table","Name","Guests","Date","Time");
        listModel.addElement(heading);
        String space =
                "----------------------------------------------------------------------------------";
        listModel.addElement(space);
        String row;
        for(ReservationSystem reservation : resList) {
            if(reservation.visible.equals("1")) {
                row = String.format("%-31s%1s%-9s%1s%-17s%-11s%-24s%s", Integer.toString(reservation.resId), " ",
                        Integer.toString(reservation.tableNumber), " ", reservation.name, Integer.toString(reservation.numOfGuests),
                        reservation.date, reservation.time);
                listModel.addElement(row);
            }
        }
        return inputList;
    }

    static JTable populateReservationsTable(JTable inputTable) {
        ArrayList<ReservationSystem> resList = connectDB.listReservations();
        String columns[] = {"Reservation Number", "Table", "Name", "Guests", "Date", "Time"};
        DefaultTableModel tableModel = new DefaultTableModel();
        tableModel.setColumnIdentifiers(columns);
        if (resList == null) {
            inputTable.setModel(tableModel);
            return inputTable;
        }
        Object[] rows = new Object[6];
        for(ReservationSystem reservation : resList) {
            if(reservation.visible.equals("1")) {
                rows[0] = Integer.toString(reservation.resId);
                rows[1] = Integer.toString(reservation.tableNumber);
                rows[2] = reservation.name;
                rows[3] = Integer.toString(reservation.numOfGuests);
                rows[4] = reservation.date;
                rows[5] = reservation.time;
                tableModel.addRow(rows);
            }
        }
        inputTable.setModel(tableModel);
        return inputTable;
    }

    static JComboBox populateResDropDown(JComboBox<String> inputBox) {
        ArrayList<ReservationSystem> resList = connectDB.listReservations();
        for(ReservationSystem reservation : resList) {
            if(reservation.visible.equals("1")) {
                inputBox.addItem(Integer.toString(reservation.resId) + " - " + reservation.name + " - " +
                        reservation.time + " - " + reservation.date + " - " + Integer.toString(reservation.tableNumber) + " - " +
                        Integer.toString(reservation.numOfGuests));
            }
        }
        return inputBox;
    }

    void setResId(int resId) {
        this.resId = resId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    void setTime(String time) {
        this.time = time;
    }

    void setTableNumber(int tableNumber) {
        this.tableNumber = tableNumber;
    }

    void setNumOfGuests(int numOfGuests) {
        this.numOfGuests = numOfGuests;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    void setVisible(String visible) { this.visible = visible; }

    public String getVisible() { return visible; }

    static int getDelResId() {
        return delResId;
    }

    static void setDelResId(int delResId) {
        ReservationSystem.delResId = delResId;
    }
}