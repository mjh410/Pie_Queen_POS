/**
 * File Name: Driver.java
 * Date: April 06, 2018
 * Author: Matt Huffman
 * Course: CMSC 495
 * Assignment: Final Project
 * Purpose: This file is the main Driver of the application and contains the main method.
 * Created Using: IntelliJ IDEA
 */

public class Driver {

    public static void main(String[] args) {

        connectDB mysql = new connectDB();
        mysql.initializeConnection();
        GUIController run = new GUIController();
    }
}
